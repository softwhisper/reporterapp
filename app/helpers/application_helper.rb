module ApplicationHelper
  
  def messages_about_actions
    messages = ""
    flash.each do |key, message|
      css_class = generate_css_class(key.to_s)
      messages << content_tag(:div, (message + link_to_close).html_safe, :class => css_class)
    end
    puts messages
    messages.html_safe
  end
  
  def link_to_remove_fields(name, f)
    f.hidden_field(:_destroy) + link_to_function(name, "remove_fields(this)")
  end

  def link_to_add_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render(association.to_s.singularize + "_fields", :builder => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  
  private
  
  def generate_css_class(key)
    css_class = 'alert-box'
    if key == 'notice'
      css_class << ' success'
    elsif key =='warning' || key == 'error'
      css_class << " #{key}"
    end
    css_class
  end
  
  def link_to_close
    content_tag(:a, "\&times;".html_safe, :class => 'close').html_safe
  end
  
end
