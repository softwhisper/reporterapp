class UsersController < ApplicationController
  before_filter :authenticate_user!
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      redirect_to edit_user_url(@user), :noitce => "El usuario fue actualizado correctamente."
    else
      render :action => 'edit'
    end
  end
end