class Admin::ApplicationsController < Admin::BaseController
  def index
    @applications = Application.all
  end

  def show
    @application = Application.find(params[:id])
  end

  def new
    @application = Application.new
    @accounts = Account.all
  end

  def edit
    @application = Application.find(params[:id])
  end

  def create
    @application = Application.new(params[:application])
    if @application.save
      redirect_to [:admin, @application], notice: 'Application was successfully created.'
    else
      render action: "new"
    end
  end

  def update
    @application = Application.find(params[:id])
    if @application.update_attributes(params[:application])
      redirect_to [:admin, @application], notice: 'Application was successfully updated.'
    else
      render action: "edit"
    end
  end

  def destroy
    @application = Application.find(params[:id])
    @application.destroy
    redirect_to admin_applications_url
  end
end
