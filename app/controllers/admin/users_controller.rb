class Admin::UsersController < Admin::BaseController
  
  def index
    @users = User.all
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def new 
    @user = User.new
    1.times do
      accounts = @user.accounts.build
      1.times { accounts.applications. build }
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def create
    @user = User.new(params[:user])
    if @user.save
      redirect_to [:admin, @user], :notice => "El usuario fue creado correctamente."
    else
      render :action => 'new'
    end
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      redirect_to [:admin, @user], :noitce => "El usuario fue actualizado correctamente."
    else
      render :action => 'edit'
    end
  end
  
  def destroy
    @user = User.find(parms[:id])
    @user.destroy
    redirecto_to admin_users_url
  end
end