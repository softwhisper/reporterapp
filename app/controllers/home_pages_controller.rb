class HomePagesController < ApplicationController
    
  before_filter :authenticate_user!, :except => :welcome
  
  def welcome
    if current_user
      redirect_to summary_url
    else
      render :layout => false
    end
  end  
    
  def summary
    get_parameters_summary
    @dates_from_14_days = get_days
    @dates_from_14_weeks = get_weeks
    @downloads = Statistics.downloads(current_user, @type_query, @date)
    @purchases = Statistics.purchases(current_user, @type_query, @date)
    @updates = Statistics.updates(current_user, @type_query, @date)
  end
  
  def details
    get_parameters_details
    @dates_from_14_days = get_days
    @dates_from_14_weeks = get_weeks
    @applications = Statistics.get_all_statistics(current_user, @type_query, @date)
  end
  
  def get_days
    days = []
    14.times do |i|
      value = (yesterday- i.day).strftime("%Y%m%d")
      display = (yesterday - i.day).strftime("%d/%m/%Y")
      days << [display, value]
    end
    days
  end
  
  def get_weeks
    weeks = []
    14.times do |i|
      value = (yesterday - i.week).strftime("%Y%m%d")
      display = (yesterday- i.week).strftime("%d/%m/%Y")
      weeks << [display, value]
    end
    weeks
  end
  
  def get_parameters_details
    if !initial_request?
      @type_query = 'weekly'
      @date = yesterday.strftime("%Y%m%d")
    else
      @type_query = params[:type_query]
      @date = params[:start_date]
    end
    cookies[:tab_active] = @type_query
  end
  
  def get_parameters_summary
    if !initial_request?
      @type_query = 'dayly'
      @date = yesterday.strftime("%Y%m%d")
    else
      @type_query = params[:type_query]
      @date = params[:start_date]
    end
    cookies[:tab_active] = @type_query
  end
  
  def initial_request?
    params[:type_query] && params[:start_date] 
  end
  
  def yesterday
    Time.now - 1.day
  end
  
end