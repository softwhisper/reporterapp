class ApplicationController < ActionController::Base
  protect_from_forgery

  def after_sign_in_path_for(resource)
    	if resource.admin?
        admin_root_url
  	  else
  	    summary_url
	    end
  end

end
