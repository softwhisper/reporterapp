class Statistics
  attr_reader :provider, :provider_country, :sku, :developer, :title, :version,
              :product_type_identifier, :units, :developer_proceeds, :begin_date,
              :end_date, :customer_currency, :country_code, :currency_of_proceeds,
              :apple_identifier, :customer_price, :promo_code, :parent_identifier,
              :subscription, :period  

  def initialize(application_values={})
    @provider = application_values["Provider"]
    @provider_country = application_values["Provider Country"]
    @sku = application_values["SKU"]
    @developer = application_values["Developer"]
    @title = application_values["Title"]
    @version = application_values["Version"]
    @product_type_identifier = application_values["Product Type Identifier"]
    @units = application_values["Units"]
    @developer_proceeds = application_values["Developer Proceeds"]
    @begin_date = application_values["Begin Date"]
    @end_date = application_values["End Date"]
    @customer_currency = application_values["Customer Currency"]
    @country_code = application_values["Country Code"]
    @currency_of_proceeds = application_values["Currency of Proceeds"]
    @apple_identifier = application_values["Apple Identifier"]
    @customer_price = application_values["Customer Price"]
    @promo_code = application_values["Promo Code"]
    @parent_identifier = application_values["Parent Identifier"]
    @subscription = application_values["Subscription"]
    @period = application_values["Period"]
  end
  
  def self.downloads(user, type_query, date)
    downloads = 0
    statistics = self.get_all_statistics(user, type_query, date)
    statistics.each do |statistic|
      if statistic.download?
        downloads +=1
      end
    end
    downloads
  end
  
  def self.updates(user, type_query, date)
    updates = 0
    statistics = self.get_all_statistics(user, type_query, date)
    statistics.each do |statistic|
      if statistic.update?
        updates +=1
      end
    end
    updates
  end
  
  def self.purchases(user, type_query, date)
    purchases = 0
    statistics = self.get_all_statistics(user, type_query, date)
    statistics.each do |statistic|
      if statistic.purchase?
        purchases +=1
      end
    end
    purchases
  end
  
  def self.get_all_statistics(user, type_query, date)
    user.accounts.each do |account|
      account.applications.each do |application|
        puts "\n\n\n type_query: #{type_query} date: #{date}"
        itca = ITCAutoingest::ITCAutoingest.new(account.username, account.password, account.vendorid.to_s, application.apple_identifier.to_s)
        if type_query == 'dayly'
          report = itca.daily_sales_summary_report(date)
        else
          report = itca.weekly_sales_summary_report(date)
        end
        @statistics = []
        if report[:error]
          #flash[:error] = report[:error]
          puts "ERROR: #{report[:error]}"
        else
          report[:report].each do |application_values|
            @statistics << Statistics.new(application_values)
          end
        end
      end
    end
    @statistics
  end

  def download?
    @product_type_identifier == "1" || @product_type_identifier == "1F" || @product_type_identifier == "1T" ||  @product_type_identifier  == "F1"
  end
  
  def purchase?
    @product_type_identifier == "IA1" || @product_type_identifier == "FI1" || @product_type_identifier == "1E" ||  @product_type_identifier == "1EP" || @product_type_identifier == "1EU"
  end
  
  def update?
    @product_type_identifier == "7" || @product_type_identifier == "7F" || @product_type_identifier == "7T" ||  @product_type_identifier == "F7"
  end
end