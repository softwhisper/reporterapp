class Account < ActiveRecord::Base
  belongs_to :user
  has_many :applications, :dependent => :destroy
  
  validates :username, :password, :vendorid, :presence => true
  
  accepts_nested_attributes_for :applications,
                                 :reject_if => lambda{ |application| application[:name].blank? || application[:apple_identifier].blank? },
                                 :allow_destroy => true
end
