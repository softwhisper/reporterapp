class Application < ActiveRecord::Base
  belongs_to :account
  
  validates :name, :apple_identifier, :presence => true
end
