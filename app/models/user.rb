class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :accounts_attributes
  
  has_many :accounts, :dependent => :destroy
  
  accepts_nested_attributes_for :accounts,
                                :reject_if => lambda{ |account| account[:username].blank? || account[:password].blank? || account[:vendorid].blank?},
                                :allow_destroy => true
end
