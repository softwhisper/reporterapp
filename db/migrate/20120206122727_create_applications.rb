class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.string :name
      t.integer :apple_identifier
      t.timestamps
    end
  end
end
